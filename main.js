function createJoinOurProgramSection() {
    const joinSection = document.createElement('div');
    joinSection.id = 'joinProgram';

    const heading = document.createElement('h2');
    heading.textContent = 'Join Our Program';

    const description = document.createElement('p');
    description.textContent = 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';

    const form = document.createElement('form');
    form.addEventListener('submit', handleFormSubmit); 

    const emailInput = document.createElement('input');
    emailInput.type = 'email';
    emailInput.name = 'email'; 
    emailInput.placeholder = 'Email';
    emailInput.classList.add('email-input');

    const subscribeButton = document.createElement('button');
    subscribeButton.type = 'submit'; 
    subscribeButton.textContent = 'Subscribe';
    subscribeButton.classList.add('subscribe-button');

    form.appendChild(emailInput);
    form.appendChild(subscribeButton);

    joinSection.appendChild(heading);
    joinSection.appendChild(description);
    joinSection.appendChild(form);

    return joinSection;
}

function handleFormSubmit(event) {
    event.preventDefault(); 
    const formData = new FormData(event.target);
    const emailValue = formData.get('email'); 
    console.log('Entered email:', emailValue); 
}

function addJoinOurProgramSection() {
    const existingContent = document.getElementById('contentToAdd');
    const joinSection = createJoinOurProgramSection();
    existingContent.insertAdjacentElement('afterend', joinSection);
}

document.addEventListener('DOMContentLoaded', addJoinOurProgramSection);
